import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Store} from "@ngrx/store";
import {CityWeather} from "../interfaces/interfaces";
import * as CityWeatherActions from "../store/city-weather.actions";

@Injectable({
  providedIn: "root"
})
export class WeatherService {

  //personal key in order to run the app
  private key: string = 'f8162d9275264bfcd438383ac5125e71'

  constructor(private http: HttpClient, private store: Store<{ citiesWeather: CityWeather[]}>) {
  }

  addCityWeatherData(cityName: string): void {
    this.http.get(
      `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${this.key}`)
      .subscribe(
        data => {
          const cityData = this.extractDataFromJson(data);
          this.store.dispatch(new CityWeatherActions.AddCity(cityData));
        },
        //future possibility - replace it with default display
        error => console.log(error)
      );
  }

  updateCityWeatherData(cityName: string): void {
    this.http.get(
      `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&units=metric&appid=${this.key}`)
      .subscribe(
        data => {
          const cityData = this.extractDataFromJson(data);
          this.store.dispatch(new CityWeatherActions.UpdateCity(cityData));
        },
        //future possibility - replace it with default display
        error => console.log(error)
      );
  }

  private extractDataFromJson(weatherData: any): CityWeather {
    //The ude of const is mainly for debugging purpose
    const temp = Math.floor(weatherData.main.temp);
    const icon = 'http://openweathermap.org/img/wn/' + weatherData.weather[0].icon +'@2x.png'
    const description = weatherData.weather[0].description
    const cityName = weatherData.name;
    return {cityName: cityName, temperature: temp, description: description, icon: icon};
  }

}
