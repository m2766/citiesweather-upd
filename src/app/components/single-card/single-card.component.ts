import {Component, Input, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import * as CityWeatherActions from "../../store/city-weather.actions";
import {WeatherService} from "../../services/weather.service";
import {CityWeather} from "../../interfaces/interfaces";

@Component({
  selector: 'app-single-card',
  templateUrl: './single-card.component.html',
  styleUrls: ['./single-card.component.scss']
})
export class SingleCardComponent implements OnInit {

  @Input() cityDataObj: CityWeather;
  
  constructor(private store: Store<{ citiesWeather: { cities : CityWeather[]}}>,
              private weatherService: WeatherService) { }

  ngOnInit(): void {
  }

  updateReq(city: CityWeather):void {
    this.weatherService.updateCityWeatherData(city.cityName);
  }

  deleteReq(city: CityWeather):void {
    this.store.dispatch(new CityWeatherActions.RemoveCity(city));
  }

}
