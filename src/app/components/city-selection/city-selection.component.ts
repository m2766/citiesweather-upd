import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import _cities from './../../../assets/cities.json';
import {map, startWith} from "rxjs/operators";
import {Observable} from "rxjs";
import {WeatherService} from "../../services/weather.service";

@Component({
  selector: 'app-city-selection',
  templateUrl: './city-selection.component.html',
  styleUrls: ['./city-selection.component.scss']
})
export class CitySelectionComponent implements OnInit {

  searchForm = new FormGroup({
    cityName: new FormControl(''),
  });
  cities = _cities.map(city => {
    return city.name;
  });
  filteredOptions$: Observable<string[]>;

  constructor(private weatherService: WeatherService) {
  }

  ngOnInit(){
    this.onValueChanged();
  }

  onValueChanged(): void{
    this.filteredOptions$ = this.searchForm.controls.cityName.valueChanges.pipe(
      startWith(''),
      map((value:any) => this._filter(value)));
  }

  private _filter(value: string): string[]{
    const filterValue = value.toLowerCase();
    return this.cities.filter(city => city.toLowerCase().includes(filterValue));
  }

  sendCityReq(): void {
    const value = this.searchForm.value.cityName;
    this.weatherService.addCityWeatherData(value);
  }
}
