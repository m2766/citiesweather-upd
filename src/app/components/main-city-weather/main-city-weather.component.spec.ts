import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCityWeatherComponent } from './main-city-weather.component';

describe('MainScreenComponent', () => {
  let component: MainCityWeatherComponent;
  let fixture: ComponentFixture<MainCityWeatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainCityWeatherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCityWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
