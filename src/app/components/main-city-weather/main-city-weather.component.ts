import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {CityWeather} from "../../interfaces/interfaces";

@Component({
  selector: 'app-main-city-weather',
  templateUrl: './main-city-weather.component.html',
  styleUrls: ['./main-city-weather.component.scss']
})
export class MainCityWeatherComponent implements OnInit {

  cities$: Observable<CityWeather[]>;

  constructor(private store: Store<{ citiesWeather: CityWeather[]}>) { }

  ngOnInit(): void {
    this.cities$ = this.store.select('citiesWeather');
  }

}
