
export interface CityWeather {
  cityName: string;
  temperature: number;
  description: string;
  icon: string;
}

