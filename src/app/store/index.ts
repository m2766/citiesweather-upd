import {cityWeatherReducer} from './city-weather.reducer';
import {ActionReducerMap} from '@ngrx/store';
import {CityWeather} from "../interfaces/interfaces";


export const rootReducer = {};

export interface AppState {
  citiesWeather: CityWeather[];
}


export const reducers: ActionReducerMap<AppState, any> = {
  citiesWeather: cityWeatherReducer
};
