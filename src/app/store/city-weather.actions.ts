import {Action} from "@ngrx/store";
import {CityWeather} from "../interfaces/interfaces";

export const ADD_CITY: string = 'ADD_CITY';
export const REMOVE_CITY: string = 'REMOVE_CITY';
export const UPDATE_CITY: string = 'UPDATE_CITY';


export class AddCity implements Action {
  readonly type = ADD_CITY;
  constructor(public payload: CityWeather) {}
}

export class RemoveCity implements Action {
  readonly type = REMOVE_CITY;
  constructor(public payload: CityWeather) {}
}

export class UpdateCity implements Action {
  readonly type = UPDATE_CITY;
  constructor(public payload: CityWeather) {}
}

export type Actions = AddCity | RemoveCity | UpdateCity;
