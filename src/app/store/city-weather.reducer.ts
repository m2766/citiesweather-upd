import * as CityWeatherActions from "./city-weather.actions";
import {CityWeather} from "../interfaces/interfaces";

const initialState: CityWeather[] = []

export function cityWeatherReducer (
  state = initialState,
  action: CityWeatherActions.Actions
): CityWeather[] {
  switch (action.type) {
    case CityWeatherActions.ADD_CITY:
      if (state.some(x => x.cityName === action.payload.cityName)) {
        return state
      } else {
        return [...state, action.payload];
      }
      case CityWeatherActions.REMOVE_CITY:
      const index = state.findIndex(x => x.cityName === action.payload.cityName);
      if (index !== -1) {
        const stateArray = [...state];
        stateArray.splice(index, 1);
        return stateArray;
      } else {
        return state;
      }
      case CityWeatherActions.UPDATE_CITY:
        const stateArray = [...state];
        stateArray.splice(
          state.findIndex(x => x.cityName === action.payload.cityName),
          1,
          action.payload);

        return stateArray;

      default :
      return state;
  }
}
